1997年5月 听力文字

Part A
1. (C). (此题录音缺)
W: Thanks a lot. This scarf (围巾) will be perfect with my blue jacket. 
M: Made a good choice, did I?
Q: What does the man mean? 
2. (B). 
W: My cousin Bob is getting married in California and I can’t decide whether to go.
M: It’s a long trip, but I think you’ll have a good time.
Q: What does the man imply? 
3. (B).
W: Excuse me, could you bring me a glass of water, please?
M: Sorry, but I am not a waiter.
Q: What does the man mean? 
4. (A).
M: Got the time?
W: It is a little after ten.
Q: What does the woman mean?
5. (D).
M: You did an excellent job on that presentation (大学里的演示课).
W: Thanks, I put a lot of time into it.
Q: What does the woman mean? 
6. (A).
M: Are you ready to go jogging?
W: Almost. I have to warm up (热身) first.
Q: What does the woman mean?
7. (C).
W: I’ve been calling David for the past half hour, but I keep getting a busy signal (忙音).
W: Well, if you don’t get him soon, we’ll just have to go to the movies without him.
Q: Why are the women trying to call David? 
8. (A).
M: If I don’t find my wallet pretty soon, I’m going to have to report it stolen.
W: Hold on (且慢). Before you call campus security office, have you checked your car, all your jacket pockets, everywhere?
Q: What does the woman suggest the man do? 
9. (C).
M: I notice that you don’t buy your lunch in the cafeteria any more.
W: When prices went up, I decided to bring my own.
Q: Why doesn’t the woman buy food in the cafeteria? 
10. (D).
M: You know my car hasn’t been the same since I bumped (撞) into that telephone pole (电话亭).
W: You’d better have that looked into (检查) before you drive to Florida.
Q: What does the woman mean? 
11. (A).
M: Hello, I’d like two seats for the evening show.
W: Sorry, but the performance’s already sold out (卖完). Would you be interested in something later this week?
Q: What does the woman imply? 
12. (D).
M: That leaking (漏水的) faucet (龙头) is starting to get to me (使我烦恼).
W: What should we do about it?
Q: What does the woman want to know? 
13. (B).
W: Could you please tell me where to find running shoes?
M: Yeah. They’d be on the second floor in sporting goods (运动用品).
Q: Where is the conversation probably taking place? 
14. (C).
M: Mary, did you drop off (寄交) the roll of film (胶卷) for developing (冲印)?
W: No, I got Susan to do it.
Q: What happened to the roll of film? 
15. (A).
W: The floor is awfully wet. What happened?
M: No sooner had I got into the shower than the phone rang (我刚进浴室电话就响了).
Q: What does the man imply? 
16. (B).
W: Aren’t you leaving tomorrow for vacation? All packed (打点好了) and ready to go?
M: Not quite. I still have to stop by the drugstore (药房) and get my allergy (过敏) prescription refilled (取药).
Q: What does the man imply? 
17. (C).
M: It’s so mild (暖和) today. Wanna go for a bike ride after your last class?
W: What’s the latest we could start. My last class is a chem-lab (= chemical laboratory) and it often runs late.
Q: What does the woman mean? 
18. (B).
M: I knew Laurie played the piano, but I didn’t know she plays the guitar.
W: Neither did I. It seems she just picked it up (学会) on her own over this summer.
Q: What does the woman mean? 
19. (D).
M: I heard that Partro-Electronics is going to be holding interviews on campus next week.
W: Yeah? What day? I’d like to talk to them and drop off (递交) my resumé.
Q: What does the woman want to do?
20. (C).
W: You know, some TV channels have been rerunning a lot of comedies from the sixties. What do you think of those old shows?
M: Not much, but then the new ones aren’t so great either.
Q: What does the man mean? 
21. (A).
M: Janet, here’s the book you loaned (= lent) me. But I’m a bit embarrassed (不好意思). Can’t seem to find the jacket (封面护套) for it.
W: I would’ve never even noticed. You are one of the few people who actually return books to me.
Q: What can be inferred from the conversation? 
22. (D).
W: Did you hear if the debate team is going on to the state competition or did they get eliminated (淘汰)?
M: Actually I haven’t been following their progress this year.
Q: What does the man imply?
23. (C).
M: I want to take comparative anatomy (比较解剖学) this year, but according to the requirements, I have to take introductory biology first.
W: Ask your professors if you can take them simultaneously (同时). All they can do is say “No.”
Q: What does the woman suggest the man do? 
24. (C).
M: If I can get Brian to pay back the money I lent him last week, I could get that new tennis racket (球拍).
W: I hope you can have better luck than I did.
Q: What does the woman imply? 
25. (B).
W: I hear you are quite proficient (熟练) on the violin.
M: I’m pretty rusty (生疏) after all these years.
Q: What does the man mean? 
26. (A).
W: I really want to see the play at the outdoor theater tonight. Will you come with me?
M: You know I hate badly all those mosquitoes (蚊子), but if you have your heart set on it (决意要去).
Q: What can be inferred about the man? 
27. (D).
W: Have you visited the new exhibit?
M: Not yet, but it will be at the student center until June.
Q: What does the man imply? 
28. (C).
M: I have to be at the dentist’s (牙医诊所) at seven thirty tomorrow morning.
W: Then you won’t miss any classes.
Q: What does the woman imply? 
29. (C).
M: I’m taking up collections (募捐) for the jazz band. Would you like to give (捐款)?
W: Just a minute while I get my wallet.
Q: What will the woman probably do next? 
30. (C).
M: Your cousins just called. They’re stranded (搁浅) at the beach (沙滩).
W: So they didn’t manage to get a lift (搭便车) after all.
Q: What had the woman assumed about her cousins? 


Part B
Questions 31 to 34. Listen to a conversation between two students:
M: Hey, Karen. Looks like you got some sun this weekend.
W: Yeah, I guess so. I spent the weekend at the beach.
M: Oh, yeah. That’s great! Where did you stay?
W: Some friends of my parents’ live out there. And they invited me for as long as I wanted to stay.
M: So what are you doing back here already?
W: Oh, I have a paper I need to work on and I just couldn’t do any serious studying at the beach.
M: I don’t blame you. So what did you do out there? I mean, besides lying on the sand obviously.
W: I jogged (慢跑) up and down the beach and I played some volleyball. You know I never realized how hard it is to run on sand. I couldn’t even get through a whole game before I had to sit down. It’s much easier to run on the wet sand near the water.
M: Not to mention (更不必说) cooler. Did you go swimming?
W: I wanted to, but they said the weather isn’t warm enough for that until a couple of months from now. So I just waded (= walked) in up to my knees.
M: It all sounds so relaxing. I wish I could get away to the beach like that.
W: It looks like you could use it. Don’t tell me you spent the weekend in the library again.
31. (A). How did the woman spend the last weekend?
32. (D). Why did the woman come home so soon?
33. (B). Why did the woman have to stop playing in the volleyball game?
34. (C). Why didn’t the woman go swimming?

Questions 35 through 38. Listen to a conversation between a graduate student and her biology professor:
M: Thanks for stopping by, Ann. I’d like to talk to you about a research project that I thought you might be interested in. A friend of mine is working in Yellow Stone National Park this summer.
W: Yellow Stone! I’ve always wanted to spend some time out of Wyoming.
M: Wait till you hear what the project is. She is working with the buffalo population. Their herds (兽群) have been increasing in size lately, which is good in theory.
W: Yeah, but I thought they were in danger of becoming extinct (灭绝). 
M: Well, apparently because of all the winter tourists, paths are created in the snow. More buffaloes are surviving the harsh (萧条) winters because the paths make it easier for the buffalo to move around and find food, but it turns out that some of the herds were infected (感染) with bacteria (细菌).
W: Oh, Yeah. I heard about that a bru….
M: A brucella abortus (牲畜的传染性流产)
W: Right. It’s been around for quite a while.
M: Yes, it has. And because the buffalo population is increasing, they’ve been roaming (漫游) more than usual. And the diseases began to spread to the cattle ranches (牛群牧场) that border (与接壤) the park.
W: That’s bad news! Isn’t that the disease that causes animals to abort (流产) their young?
M: Yeah. And it caused a lot of controversy. Some of the ranchers (牧场主) even want to destroy the buffalo herds.
W: That’s awful. Have they made much progress with the research?
M: So far they’ve been collecting tissue samples from dead buffalo to see if bacteria is present 
W: I’d really be interested in working on this. You know I’ve been researching diseased animal population.
M: That’s why I thought of you. I took the liberty (冒昧) of mentioning your name to my friend. She’s hoping you’ll be able to spend the whole summer out there.
W: Well, I was going to work on my thesis (硕士论文) a lot in July, but I’m sure my advisor wouldn’t want me to pass up (放过) this opportunity.
35. (D). What did the professor want to talk to Ann about?
36. (A). According to the professor, why is the buffalo population increasing?
37. (C). Why does the professor think Ann would be interested in going to Yellow Stone?
38. (A). How will Ann probably spend the summer?


Part C
Questions 39 through 41. Listen to a talk given by a tour guide:
Welcome to Everglade National Park. The Everglade’s a watery plain covered with saw grass (锯叶草) that’s home to numerous species of plants and wildlife at one and a half million acres that’s too big to see it all today, but this tour will offer you a good sampling (抽样). Our tour bus will stop first at Taylorsloo. This is a good place to start because it’s home to many of the plants and animals typically associated (与联系) with the Everglade. You’ll see many exotic (奇异的) birds and, of course, the world famous alligators (鳄鱼). Don’t worry. There’s a board walk that goes across the marsh (沼泽), so you can look down at the animals in the water from a safe distance. The board walk is high enough to give you a great view of the saw grass prairie (草原). From there we’ll head for some other marshy (沼泽的) and even jungle-like (森林样的) areas that feature wonderful tropical plant life. For those of you who’d want a closer view of saw grass prairie, you might consider running a canoe (独木舟) sometime during your visit here. However, don’t do this unless you have a very good sense of direction and can negotiate your way (找路) through tall grass. We hate to have to come looking for you. You have a good fortune of being here in the winter, the best time of the year to visit. During the spring and summer, the mosquitoes were just about to eat you alive (生吃). Right now they are not so bothersome but you’ll still want to use insect repellent (驱虫剂).
39. (B). What’s the main purpose of the tour?
40. (A). What does the speaker imply about paddling (用桨划) across the water in a canoe?
41. (D). Why is it good to visit the Everglade in the winter?

Questions 42 to 46. Listen to a talk given by an astronaut (宇航员):
Thank you. It’s great to see so many of you interested in this series on survival in outer space. Please excuse the cameras. We’re being video-taped (录像) for the local TV stations. Tonight I’m going to talk about the most basic aspect of survival, the space suit (宇航服). When most of you imagine an astronaut (宇航员), that’s probably the first thing that comes to mind, right? 
Well, without space suit it will not be possible for us to survive in space. For example, outer space is a vacuum (真空). There’s no gravity (重力) or air pressure. Without protection, our bodies will explode. What’s more, we cook in the sun (晒焦) or freeze (冻僵) in the shade (阴影) with temperatures ranging from a toasty (烘烤的) 300 degrees above to a cool 300 degrees below zero Fahrenheit. The space suit that NASA has developed is truly a marvel (奇迹). This photo enlargement (放大物) here is a life-size (实际尺寸的) image of an actual size space suit worn by astronauts on the last space shuttle mission (穿梭任务). This part is the torso (躯干). It’s made of seven extremely durable (耐用的) layers. This thick insulation protects against temperature extremes and radiation (辐射). Next is what they call a bladder (气囊) of oxygen. That’s an inflatable (充气的) sack filled with oxygen to simulate (模仿) atmosphere pressure. This bladder presses against the body with the same force as the Earth’s atmosphere at sea level. The innermost (最里面的) layers provide liquid cooling (液体冷却) and ventilation (空气流通). Despite all the layers the suit is flexible, allowing free movement so we can work. Another really sophisticated (先进的) part of the space suit is the helmet (头盔). I brought one along to show you. Can I have a volunteer come and demonstrate (演示)?
42. (D). What’s the speaker’s main purpose?
43. (A). What would cause an unprotected human body to explode in outer space?
44. (B). Where is the bladder of oxygen located?
45. (C). What does the speaker show the audience as she described the main part of the space suit?
46. (C). What will probably happen next?

Questions 47 through 50. Listen to a talk about a program sponsored by a student organization:
Good evening, my name is Pam Johns and on behalf of (代表) the modern dance club, I’d like to welcome you to tonight’s program. The club is pleased to present (放映) the TV version (版本) of the Catherine Wheel, Twyla Tharp’s rock ballet (摇滚芭蕾). 
This video version of the ballet has been even more successful with audiences than the original theater production (演出). It includes some animations (动画), slow motion (慢影动作) and stop action freezes (定格) that really help the audiences understand the dance. The title of the piece refers to Saint Catherine who died on a wheel in 307 AD. Nowadays a Catharine Wheel is also a kind of firework (焰火) that looks something like a pinwheel. Anyway the dance is certainly full of fireworks. You’ll see how Twyla Tharp explores one family’s attempt to confront (面对) the violence in modern life. The central symbol of the work is a pineapple (菠萝). But exactly what it represents has always created a lot of controversy (争论). As you watch, see if you can figure it out. 
The music for this piece is full of the rhythmic (有节奏的) energy of the rock music. It was composed by David Burn of the rock band Talking Head. And the lead dancer in this version was Sarah Rodner who is perfectly suited to (适合于) Tharp’s adventurous choreography (舞蹈编导). Following the video, dance teacher Mary Parker will lead a discussion about the symbolism Mrs. Tharp used. We hope you can stay for that. So enjoy tonight’s video and thank you for your support.
47. (C). What’s the purpose of the talk?
48. (C). Why was the video version of the dance more successful than the theater production?
49. (D). What kind of music is the dance performed to?
50. (A). What will probably be included in the discussion after the program?

